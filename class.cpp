#include<iostream>
#include<string>
#include<sstream>

using namespace std;
///This class prints student's name, surname, age and standard. 
///It contains set and get functions and string_to_string() function.
class Student
{
private:
	int age, standard;
	string name, surname;
public:
	void set_age(int a) {
		age = a;
	}
	int get_age() {
		return age;
	}
	void set_name(string n) {
		name = n;
	}
	string get_name() {
		return name;
	}
	void set_surname(string s) {
		surname = s;
	}
	string get_surname() {
		return surname;
	}
	void set_standard(int st) {
		standard = st;
	}
	int get_standard() {
		return standard;
	}
	///This class function seperate the string like "text,text,text.."
	string to_string() {
		stringstream ss;
		char c = ',';
		ss << age << c << name << c << surname << c << standard;
		return ss.str();
	}
};
int main()
{
	Student st;
	int age, standard;
	string name, surname;

	cin >> age >> name >> surname >> standard;
	st.set_age(age);
	st.set_name(name);
	st.set_surname(surname);
	st.set_standard(standard);

	cout << st.get_age() << endl << st.get_surname() << ", " << st.get_name() << endl << st.get_standard() << endl<<endl;
	cout << st.to_string();
	cout << endl << endl;
	system("pause");
}