#include<iostream>
#include<string>

using namespace std;
/// it takes 2 string from user and calculates size of them, combines them and swaps first letters.
int main()
{
	string a, b;

	cin >> a >> b;

	cout << a.size() << "\t" << b.size() << endl;
	cout << a + b << endl;

	swap(a[0], b[0]);

	cout << a << "\t" << b;

	cout << endl << endl;
	system("pause");
}