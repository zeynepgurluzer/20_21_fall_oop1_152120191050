#include<iostream>
using namespace std;
int main()
{
	int N, i;
	
	cin >> N;
	if (N > 1000 || N <= 0)
		cout << "please don't enter greater than 1000 or smaller than 0! ";
	else
	{
		int *ar = new int[N];
		for (i = 0; i < N; i++)
		{
			
			cin >> ar[i];
		}
		for (i = N - 1; i >= 0; i--)
		{
			cout << ar[i] << "\t";
		}
		delete[] ar;
		ar = nullptr;
	}
	cout << endl << endl;
	system("pause");
}