#include<iostream>
#include<iomanip>
#include<fstream>
#include<string>
using namespace std;

void calculate_sum_and_avr(int *dynamic_array, int count_of_int);
//dizideki t�m elemanlar� �arpar ve ekrana basar.
void calculate_product(int *dynamic_array, int count_of_int);
//dizideki en k���k eleman� bulur ve ekrana basar.
void calculate_smallest(int *dynamic_array, int count_of_int);
int main()
{
	fstream data_file;
	string filename;
	int count_of_int, counter = 0;
	cout << "Please enter text file name: ";
	getline(cin, filename);
	data_file.open(filename, ios::in); //dosya a�ma modunda a��ld�.

	if (!data_file) //e�er dosya a��lmazsa hata verecek.
	{
		cout << "The file can not be opened.\n";
		exit(0);
	}

	data_file >> count_of_int; //integer say�s� al�nd�.

	int *dynamic_array = new int[count_of_int]; //dinamik dizi olu�turuldu.
	while (!data_file.eof()) //.txt dosyas� bitene kadar okuma yap�lacak.
	{
		data_file >> dynamic_array[counter];
		cout << dynamic_array[counter] << "\t";
		counter++;
	}

	calculate_sum_and_avr(dynamic_array, count_of_int);
	calculate_product(dynamic_array, count_of_int);
	calculate_smallest(dynamic_array, count_of_int);

	delete[] dynamic_array;
	data_file.close();
	cout << endl << endl;
	system("pause");
}
void calculate_sum_and_avr(int *dynamic_array, int count_of_int)
{
	int sum = 0, i;
	float average;
	for (i = 0; i < count_of_int; i++)
	{
		sum += dynamic_array[i];
	}
	average = (float)sum / count_of_int;

	cout << endl << endl << "Sum is " << sum << endl;
	cout << "Average is " << average << endl;
}
void calculate_product(int *dynamic_array, int count_of_int)
{
	int i, product = 1;
	for (i = 0; i < count_of_int; i++)
	{
		product *= dynamic_array[i];
	}
	cout << "Product is " << product << endl;
}
void calculate_smallest(int *dynamic_array, int count_of_int)
{
	int i, smallest;
	smallest = dynamic_array[0];
	for (i = 1; i < count_of_int; i++)
	{
		if (dynamic_array[i] < smallest)
			smallest = dynamic_array[i];
	}
	cout << "Smallest is " << smallest << endl;
}