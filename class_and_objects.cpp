#include<iostream>

using namespace std;
///This class calculates students's scores. 
///Calculates how many students get higher than Kristen
class Student {
private:
	int **scores, n, *ar;//n=input(row)
public:
	void set(int in) {
		int **s = new int*[in];
		
		for (int i = 0; i < in; i++) {
			s[i] = new int[5];
		}	

		int *a = new int[in];
		scores = s;
		ar = a;
	}
	///Memory allocation for **scores
	///I used dynamic matrix for scores.
	void input() {
		cin >> n;
		set(n);
		for (int i = 0; i < n; i++)	{
			for (int j = 0; j < 5; j++) {
				cin >> scores[i][j];
			}
		}
	}
	///It compares the scores.
	int calculate_total_score() {
		int kristen, counter=0;
		input();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 5; j++) {
				ar[i] += scores[i][j];
			}
		}
		kristen = ar[0];
		for (int i = 1; i < n; i++) {
			if (kristen < ar[i])
				counter++;
		}
		return counter;
	}
};
int main()
{
	Student st;
	int result;
	result = st.calculate_total_score();
	cout << result;
	cout << endl << endl;
	system("pause");
}